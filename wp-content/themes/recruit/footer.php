<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package recruit
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer_inner">
			<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<p class="footer_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/logo.png"></a></p>
				<?php
			else :
				?>
				<p class="footer_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/logo.png"></a></p>
			<?php endif; ?>
			</div><!-- .site-branding -->
			<ul>
				<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/company/">会社情報</a></li>
				<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/interview/">スタッフインタビュー</a></li>
				<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/information/">採用情報</a></li>
				<li class="page_item page-item-2"><a href="#">エントリー</a></li>
			</ul>
		</div>
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
	$(function(){
			$('#nav_toggle').click(function(){
						$("header").toggleClass('open');
				$("nav").slideToggle(500);
					});

		});
</script>
</body>
</html>
