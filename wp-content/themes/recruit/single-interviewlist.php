<?php
get_header();
?>
<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <section class="title_wrap post-item">
      <h2>INTERVIEW<br><span><?php the_field('text-top'); ?></span></h2>
    </section>
		
		<div class="main_sub staffinterview_box">
			<section class="contents_wrap detail_profile">
				<div class="img_wrap">
					<img src="<?php the_field('photo-icon'); ?>" alt="<?php the_title(); ?>">
				</div>
				<div class="text_wrap">
					<div class="inner">
						<h3><?php the_title(); ?></h3>
						<p><?php the_field('text-job'); ?></p>
						<p><?php the_field('text-time'); ?></p>
						<p><?php the_field('text-profile'); ?></p>
					</div>
				</div>
			</section>
			
			<section class="contents_wrap box_interview">
				<?php if(post_custom('photo-icon_text1')): // 入力がある場合 ?>
				<div class="img_wrap">
					<img src="<?php the_field('photo-icon_text1'); ?>" alt="">
				</div>
				<div class="text_wrap">
					<h4><?php the_field('text-header1'); ?></h4>
					<p><?php the_field('text-interview1'); ?></p>
				</div>
				<?php else: // 入力がない場合 ?>
					<h4><?php the_field('text-header1'); ?></h4>
					<p><?php the_field('text-interview1'); ?></p>
				<?php endif; ?>
			</section>
			
			<section class="contents_wrap box_interview">
				<?php if(post_custom('photo-icon_text2')): // 入力がある場合 ?>
				<div class="img_wrap">
					<img src="<?php the_field('photo-icon_text2'); ?>" alt="">
				</div>
				<div class="text_wrap">
					<h4><?php the_field('text-header2'); ?></h4>
					<p><?php the_field('text-interview2'); ?></p>
				</div>
				<?php else: // 入力がない場合 ?>
					<h4><?php the_field('text-header2'); ?></h4>
					<p><?php the_field('text-interview2'); ?></p>
				<?php endif; ?>
			</section>
			
			<section class="contents_wrap box_interview">
				<?php if(post_custom('photo-icon_text3')): // 入力がある場合 ?>
				<div class="img_wrap">
					<img src="<?php the_field('photo-icon_text3'); ?>" alt="">
				</div>
				<div class="text_wrap">
					<h4><?php the_field('text-header3'); ?></h4>
					<p><?php the_field('text-interview3'); ?></p>
				</div>
				<?php else: // 入力がない場合 ?>
					<h4><?php the_field('text-header3'); ?></h4>
					<p><?php the_field('text-interview3'); ?></p>
				<?php endif; ?>
			</section>
		</div>
  <?php endwhile; ?>
<?php else : ?>
  <div class="error">
    <p>お探しの記事は見つかりませんでした。</p>
  </div>
<?php endif; ?>
<?php
get_footer();
?>