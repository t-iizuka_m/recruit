<?php
get_header();
?>
<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <section class="title_wrap post-item">
      <h2>ABOUT<br><span><?php the_title(); ?></span></h2>
    </section>
		
		<div class="main_sub information_box">
			<?php the_content(); ?>
		</div>
  <?php endwhile; ?>
<?php else : ?>
  <div class="error">
    <p>お探しの記事は見つかりませんでした。</p>
  </div>
<?php endif; ?>
<?php
get_footer();
?>