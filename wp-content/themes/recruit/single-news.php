<?php
get_header();
?>
<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <section class="title_wrap title_wrap_news post-item">
      <h2>NEWS<br><span><?php the_title(); ?></span></h2>
    </section>
		
		<div class="main_sub blog_box">
			<div class="inner">
				<img src="<?php the_post_thumbnail_url(); ?>"/>
				<div class="text_wrap">
					<?php the_content(); ?>
						<?php 

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			endwhile; ?>
				</div>
				<div class="navi-box">
										<?php
$prevpost = get_adjacent_post(true, '', true); //前の記事
$nextpost = get_adjacent_post(true, '', false); //次の記事
if( $prevpost or $nextpost ){ //前の記事、次の記事いずれか存在しているとき
?>

<?php
if ( $prevpost ) { //前の記事が存在しているとき
echo '<div class="navi_bottom"><div class="eyecatchbox">' . get_the_post_thumbnail($prevpost->ID, 'thumbnail') . '</div><div class="navi-detail"><a href="' . get_permalink($prevpost->ID) . '">' . get_the_title($prevpost->ID) . '</a></div></div>';
}
if ( $nextpost ) { //次の記事が存在しているとき
echo '<div class="navi_bottom"><div class="eyecatchbox">' . get_the_post_thumbnail($nextpost->ID, 'thumbnail') . '</div><div class="navi-detail"><a href="' . get_permalink($nextpost->ID) . '">' . get_the_title($nextpost->ID) . '</a></div></div>';
}
?>
<?php } ?>
				</div>
			</div>

<?php else : ?>
  <div class="error">
    <p>お探しの記事は見つかりませんでした。</p>
  </div>
<?php endif; ?>
		</div>
<?php
get_footer();
?>