<?php
get_header();
?>
<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <section class="title_wrap title_wrap_news post-item">
      <h2>NEWS<br><span><?php the_title(); ?></span></h2>
    </section>
		
		<div class="main_sub blog_box">
			<div class="inner">
				<img src="<?php the_post_thumbnail_url(); ?>"/>
				<div class="text_wrap">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="contents_wrap">
	<?php 
	function nendebcom_add_img_next_prev_post_link( $output, $format, $link, $post, $adjacent ){
 
    if( has_post_thumbnail( $post ) ){
        // ①アイキャッチ画像    thumbnail,medium
        $post_thumbnail_img = get_the_post_thumbnail( $post, 'thumbnail' );
    }else{
        // ②画像が無い場合のダミー画像
        $post_thumbnail_img = '<img src="' . includes_url( 'images/blank.gif' ) . '" alt="" />';
    }
    // ③画像タグを <div> の中に入るように置換する
    $output = str_replace( '<div class="nav-' . $adjacent . '">', '<div class="nav-' . $adjacent . '">' . $post_thumbnail_img, $output );
 
    return $output;
}
add_filter( 'next_post_link', 'nendebcom_add_img_next_prev_post_link', 10, 5 );
add_filter( 'previous_post_link', 'nendebcom_add_img_next_prev_post_link', 10, 5 );

	?>
	  <?php the_post_navigation( array(
    'prev_text'           => '前の記事 - %title',
    'next_text'           => '次の記事 - %title',
    'screen_reader_text'  => '前後の記事へのリンク',
  ) ); 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			endwhile; ?>
<?php else : ?>
  <div class="error">
    <p>お探しの記事は見つかりませんでした。</p>
  </div>
<?php endif; ?>
</div>
		</div>
<?php
get_footer();
?>