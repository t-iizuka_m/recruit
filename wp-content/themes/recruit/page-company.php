<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package recruit
 */

get_header();
?>

	<div id="primary" class="content-area">
		<section class="title_wrap post-item title_wrap_about">
      <h2>COMPANY<br><span><?php the_title(); ?></span></h2>
    </section>
		<main class="main_sub site-main company_page">
		<?php
		while ( have_posts() ) :the_post();?>
			<section class="contents_wrap">
				<h3>会社概要</h3>
				<table class="tblbox">
					<tr>
						<th>会社名</th>
						<td><?php the_field('company-profile-name'); ?></td>
					</tr>
					<tr>
						<th>設立</th>
						<td><?php the_field('company-profile-establishment'); ?></td>
					</tr>
					<tr>
						<th>資本金</th>
						<td><?php the_field('company-profile-capital'); ?></td>
					</tr>
					<tr>
						<th>社員数</th>
						<td><?php the_field('company-profile-number_of_employees'); ?></td>
					</tr>
					<tr>
						<th>役員</th>
						<td><?php the_field('company-profile-officer'); ?></td>
					</tr>
					<tr>
						<th>事業内容</th>
						<td><?php the_field('company-profile-business_content'); ?></td>
					</tr>
				</table>
			</section>
			
			<?php 
			$table = get_field( 'company-profile-table' );

if ( $table ) {

    echo '<table border="0">';

        if ( $table['header'] ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $table['header'] as $th ) {
											 if ($th == reset($table['header'])) {
        // 最初
												 echo '<th>';
                            echo $th['c'];
                        echo '</th>';
    }else{
											echo '<td>';
                            echo $th['c'];
                        echo '</td>';	 
											 }
                        
                    }

                echo '</tr>';

            echo '</thead>';
        }

        echo '<tbody>';

            foreach ( $table['body'] as $tr ) {

                echo '<tr>';

                    foreach ( $tr as $td ) {

                       if ($td == reset($tr)) {
        // 最初
												 echo '<th>';
                            echo $td['c'];
                        echo '</th>';
    }else{
											echo '<td>';
                            echo $td['c'];
                        echo '</td>';	 
											 }
                    }

                echo '</tr>';
            }

        echo '</tbody>';

    echo '</table>';
}
			?>
			
			<section class="contents_wrap access">
				<h3>アクセス</h3>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12967.05887887352!2d139.6986262!3d35.658168!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x55fd823d67ab78c2!2z5qCq5byP5Lya56S-44Oh44Ks77yI44Oh44Ks44Kw44Or44O844OX77yJ!5e0!3m2!1sja!2sjp!4v1559183100155!5m2!1sja!2sjp" frameborder="0" style="border:0" allowfullscreen></iframe>
				<p><?php the_field('company-profile-access-text'); ?></p>
				<div class="office_image">
					<img src="<?php the_field('company-profile-access-img1'); ?>" alt="オフィスイメージ">
					<img src="<?php the_field('company-profile-access-img2'); ?>" alt="オフィスイメージ">
				</div>
			</section>
			
			
			
		<?php	
		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
?>