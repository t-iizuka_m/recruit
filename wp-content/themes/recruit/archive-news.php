<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package recruit
 */

get_header();
?>

	<div id="primary" class="content-area">
	<section class="title_wrap post-item title_wrap_news">
		<h2>NEWS<br><span>新着情報</span></h2>
	</section>
		<main class="main_sub site-main archive_news">

		<?php if ( have_posts() ) : ?>
			<section class="archive_newslist contents_wrap">
				<!--<div class="headlineComment">
					<h3>みんなの共通の思いは<br class="dn">「誰かの役に立ちたい」ということ。</h3>
					<p>ダミーテキストダミーテキストダミーテキストダミーテキスト<br class="dn">ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
				</div>-->
				<div class="inner">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :the_post();?> 		
				<div class="list">
					<a href="<?php the_permalink(); ?>">
					<?php if(has_post_thumbnail()): ?>
						<img src="<?php the_post_thumbnail_url(); ?>"/>
					<?php endif; ?>
					<div class="textbox">
						<p class="item_text"><?php the_field('text-top'); ?></p>
						<p class="item_name"><?php the_title(); ?></p>
					</div>
					</a>
				</div>
			<?php endwhile;?>
				</div>
				<?php
			wp_pagenavi();
?>
			</section>
			<?php the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
