<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package recruit
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="mainbanner_wrapper">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/top/mv.jpg" alt="メインビジュアル">
			</div>
			
			<div class="companymessage">
				<div class="inner">
					<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/logo.png"></p>
					<p><span>人をもっと笑顔に「ワクワク」を思いにする会社</span></p>
					<p>ひとりひとりが丁寧で完成度の高いアートワークを心がけ「人を笑顔にする仕事」をし続けていきたいと考えています。チーム全体が根本的課題の解決をミッションと捉え、見た目のキレイさだけではなく、クリエイター視点での固定観念を排除し、クライアントのコンテクストをよく理解し様々な角度からアプローチすることで新しいコンテンツを創造し続けていきます。常に明るく前向きに、素直な心を持って仕事に打ち込めるスペシャリスト集団を私たちは目指しています。</p>
				</div>
			</div>
			
			<section class="section_top_about">
				<div class="section_top_header">
					<div class="borderbox">
						<h2>私たちの会社を知る</h2>
					</div>
				</div>
				<div class="top_about_list">
				<?php
					$args = array(
						'post_type' => 'about',
						'posts_per_page' => 6
					);
					$st_query = new WP_Query( $args );
				?>
				<?php if ( $st_query->have_posts() ): ?>
  			<?php while ( $st_query->have_posts() ) : $st_query->the_post(); ?>
					<div class="top_about_item">
						<a href="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()): ?>
							<img src="<?php the_post_thumbnail_url(); ?>"/>
						<?php endif; ?>
							<div class="top_about_textbox">
								<p class="top_about_item_name"><?php the_title(); ?></p>
								<p><?php the_field('about-text-top'); ?></p>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
				<?php else: ?>
					<p>新しい記事はありません</p>
				<?php endif; ?>
				</div>
			</section>
			
			<section class="section_top_staff">
				<div class="section_top_header">
					<div class="borderbox">
						<h2>スタッフインタビュー</h2>
					</div>
				</div>
				
				<div class="top_staff_list">
					<div class="list">
						<?php
					$args = array(
						'post_type' => 'interview',
						'posts_per_page' => 3
					);
					$st_query = new WP_Query( $args );
				?>
				<?php if ( $st_query->have_posts() ): ?>
  			<?php while ( $st_query->have_posts() ) : $st_query->the_post(); ?>
					
					<div class="top_staff_item">
						<a href="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()): ?>
							<img src="<?php the_post_thumbnail_url(); ?>"/>
						<?php endif; ?>
						<div class="top_staff_item_textbox">
							<p class="top_staff_item_text"><?php the_field('text-top'); ?></p>
							<p class="top_staff_item_name"><?php the_title(); ?></p>
						</div>
						</a>
					</div>
					
				<?php endwhile; ?>
				<?php else: ?>
					<p>新しい記事はありません</p>
				<?php endif; ?>
					</div>
					<p class="btn-flat-border-outer"><a href="index.php/interview/" class="btn-flat-border">VIEW MORE</a></p>
				</div>
			</section>
			
			<section class="section_top_blog">
				<div class="section_top_header">
					<div class="borderbox">
						<h2>新着情報</h2>
					</div>
				</div>
				<div class="top_blog_list">
					<div class="list">
				<?php
					$args = array(
						'post_type' => 'news',
						'posts_per_page' => 3
					);
					$st_query = new WP_Query( $args );
				?>
				<?php if ( $st_query->have_posts() ): ?>
  			<?php while ( $st_query->have_posts() ) : $st_query->the_post(); ?>
					<div class="top_blog_item">
						<a href="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()): ?>
							<img src="<?php the_post_thumbnail_url(); ?>"/>
						<?php endif; ?>
						<div class="top_blog_item_text">
							<p><?php the_time('Y年n月j日'); ?>｜<?php $catgory = get_the_category(); $cat_name = $catgory[0]->cat_name; echo $cat_name; ?></p>
							<p><?php the_title(); ?></p>
						</div>
						</a>
					</div>
				<?php endwhile; ?>
				<?php else: ?>
					<p>新しい記事はありません</p>
				<?php endif; ?>
					</div>
					<p class="btn-flat-border-outer"><a href="index.php/news/" class="btn-flat-border">VIEW MORE</a></p>
				</div>
			</section>
			
			<section class="section_top_banner">
				<div class="banner">
					<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/top/newgrads.jpg" alt="新卒採用エントリー"></a>
					<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/top/career.jpg" alt="中途採用エントリー"></a>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
?>