<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package recruit
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<!-- 先にjQueryを読み込む記述を書く --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'recruit' ); ?></a>
	
	<header>
		<div class="inner clearfix">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/image/logo.png"></a></h1>
			<div id="nav_toggle">
				<div>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<nav>
				<ul>
					<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/company/">会社情報</a></li>
					<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/interview/">スタッフインタビュー</a></li>
					<li class="page_item page-item-2"><a href="https://recruit.svtest.jp/index.php/information/">採用情報</a></li>
					<li class="page_item page-item-2"><a href="#"><span class="entrybtn">エントリー</span></a></li>
				</ul>
			</nav>
		</div>
	</header>

	<div id="content" class="site-content">
