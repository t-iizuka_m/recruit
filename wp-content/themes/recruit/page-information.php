<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package recruit
 */

get_header();
?>

	<div id="primary" class="content-area">
		<section class="title_wrap post-item title_wrap_about">
      <h2>INFORMATION<br><span><?php the_title(); ?></span></h2>
    </section>
		<main class="main_sub site-main information_page">
		<?php
		while ( have_posts() ) :the_post();?>
			<section class="contents_wrap">
				<h3>募集要項</h3>
				<table class="tblbox">
					<tr>
						<th>応募職種</th>
						<td><?php the_field('information-jobType1'); ?></td>
					</tr>
					<tr>
						<th>初任給</th>
						<td><?php the_field('information-startingSalary'); ?></td>
					</tr>
					<tr>
						<th>諸手当</th>
						<td><?php the_field('information-variousBenefits'); ?></td>
					</tr>
					<tr>
						<th>昇給</th>
						<td><?php the_field('information-raise'); ?></td>
					</tr>
					<tr>
						<th>賞与</th>
						<td><?php the_field('information-bonus'); ?></td>
					</tr>
					<tr>
						<th>休日休暇</th>
						<td><?php the_field('information-holidayVacation'); ?></td>
					</tr>
					<tr>
						<th>福利厚生</th>
						<td><?php the_field('information-welfare'); ?></td>
					</tr>
					<tr>
						<th>勤務地</th>
						<td><?php the_field('information-work-location'); ?></td>
					</tr>
					<tr>
						<th>勤務時間</th>
						<td><?php the_field('information-working-hours'); ?></td>
					</tr>
				</table>
				
				<section class="section_top_banner">
					<div class="banner">
						<a href="<?php the_field('information-entryBanner1-link'); ?>"><img src="<?php the_field('information-entryBanner1'); ?>"></a>
						<a href="<?php the_field('information-entryBanner2-link'); ?>"><img src="<?php the_field('information-entryBanner2'); ?>"></a>
					</div>
				</section>
			</section>
		<?php	
		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
?>